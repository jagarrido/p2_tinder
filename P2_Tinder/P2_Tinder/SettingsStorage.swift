//
//  SettingsStorage.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 18/5/21.
//

import Foundation
import Combine

enum ObjectSavableError: String, LocalizedError {
    case unableToEncode = "Unable to encode object into data"
    case noValue = "No data object found for the given key"
    case unableToDecode = "Unable to decode object into given type"
    case unableToGetCars = "Imposible obtener un listado de coches favoritos"
    
    var errorDescription: String? {
        rawValue
    }
}

final class SettingsStorage: ObservableObject {
    
    @Published var defaults: UserDefaults
    
    init(defaults: UserDefaults = .standard) {
        self.defaults = defaults
        
        defaults.register(defaults: [
            "app.view.settings.showPurchasedOnly" : false,
            "app.view.settings.showFavedOnly" : false,
            "app.view.settings.cars" : [String](),
            "app.view.settings.favourites.cars" : [String](),
        ])
        
    }

    var showPurchasedOnly: Bool {
        get {
            defaults.bool(forKey: "app.view.settings.showPurchasedOnly")
        }
        set {
            defaults.set(newValue, forKey: "app.view.settings.showPurchasedOnly")
        }
    }
    
    var showFavedOnly: Bool {
        get {
            defaults.bool(forKey: "app.view.settings.showFavedOnly")
        }
        set {
            defaults.set(newValue, forKey: "app.view.settings.showFavedOnly")
        }
    }
    
    func getSavedCars() -> [String] {
        return defaults.object(forKey: "app.view.settings.cars") as? [String] ?? [String]()
    }

    func saveCar(carName: String) {
        var savedCars = self.getSavedCars()
        savedCars.append(carName)
        defaults.set(savedCars, forKey: "app.view.settings.cars")
    }
    
    func contains(carName: String) -> Bool {
        let cars = self.getSavedCars()
        return cars.contains(carName)
    }
    
    func getFavouritesCars() -> [String] {
        return defaults.object(forKey: "app.view.settings.favourites.cars") as? [String] ?? [String]()
    }
    
    func setFavouriteCar(car: Car) {
        var favouritesCars = self.getFavouritesCars()
        favouritesCars.append(car.name)
        defaults.set(favouritesCars, forKey: "app.view.settings.favourites.cars")
        
        do {
            
            var cars = try self.getObject(forKey: "app.view.settings.favourites.carObject", castTo: [Car].self)
            cars.append(car)
            try self.setObject(cars, forKey: "app.view.settings.favourites.carObject")

        } catch {
            var cars = [Car]()
            cars.append(car)
            do {
                try self.setObject(cars, forKey: "app.view.settings.favourites.carObject")
            } catch {
//                throw ObjectSavableError.unableToGetCars
            }
//            throw ObjectSavableError.unableToGetCars
        }
        
    }
    
    func getFavouritesCarsObjects() -> [Car]? {
        do {
            let car = try self.getObject(forKey: "app.view.settings.favourites.carObject", castTo: [Car].self)
            return car
        } catch {
            return nil
        }
    }
    
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable {
        let encoder = JSONEncoder()
        do {
            let data = try encoder.encode(object)
            defaults.set(data, forKey: forKey)
        } catch {
            throw ObjectSavableError.unableToEncode
        }
    }
    
    func getObject<Object>(forKey: String, castTo type: [Object].Type) throws -> [Object] where Object: Decodable {
        guard let data = defaults.data(forKey: forKey) else { throw ObjectSavableError.noValue }
        let decoder = JSONDecoder()
        do {
            let object = try decoder.decode(type, from: data)
            return object
        } catch {
            throw ObjectSavableError.unableToDecode
        }
    }
    
    func isFavourite(carName: String) -> Bool {
        let cars = self.getFavouritesCars()
        return cars.contains(carName)
    }
    
}
