//
//  MenuView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 30/5/21.
//

import SwiftUI

struct MenuView: View {
    
    let width: CGFloat
    let isOpen: Bool
    let menuClose: () -> Void
    
    var body: some View {
        ZStack {
            GeometryReader { _ in
                EmptyView()
            }
            .background(Color.gray.opacity(0.3))
            .opacity(self.isOpen ? 1.0 : 0.0)
            .animation(Animation.easeIn.delay(0.25))
            .onTapGesture {
                self.menuClose()
            }
            
            HStack {
                MenuContent()
                    .frame(width: self.width)
                    .background(Color.white)
                    .offset(x: self.isOpen ? 0 : -self.width)
                    .animation(.default)
                
                Spacer()
            }
        }
    }
}

//struct MenuView_Previews: PreviewProvider {
//    static var previews: some View {
//        MenuView(width: 280, isOpen: true)
//    }
//}

struct MenuContent: View {
    
    var body: some View {
        List {
            Text("Aprendido durante el curso")
                .font(.system(.title, design: .rounded))
                .bold()
            HStack {
                Image(systemName: "1.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Crear el diseño e implementación de tu app que quieras utilizando la nueva librería Swift UI")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "2.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Hacer un portfolio para poder trabajar como desarrollador iOS freelancer desde casa!")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "3.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Crear tu propio portfolio de apps para aplicar a empresas de tecnología como desarrollador de apps junior")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "4.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Qué es el MVVM y cómo se puede implementar en Swift")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "5.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Arrancar tu negocio de creación de apps y emprender!")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "6.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Dominar el diseño de aplicaciones desde el layout, el mockup y el prototipado de tu idea con Swift UI")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "7.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("Aprender a trabajar con el nuevo framework de Apple para UI: SwiftUI")
                    .font(.system(.body, design: .rounded))
            }
            HStack {
                Image(systemName: "8.circle.fill")
                    .font(.system(size: 24))
                    .foregroundColor(Color(.systemTeal))
                Text("El framework de Combine para responder a cambios en la UI y en el modelo de datos automáticamente")
                    .font(.system(.body, design: .rounded))
            }
            
        }
    }
}
