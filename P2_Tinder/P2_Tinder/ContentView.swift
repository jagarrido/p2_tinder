//
//  ContentView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 08/05/2021.
//

import SwiftUI

struct ContentView: View {
    
    @State var currentCar: Car?
    
    private let threshold: CGFloat = 10
    
    @GestureState private var dragState = DragState.none
    
    @State private var lastCardIdx = 1
    @State var deck : [CardView] = {
        
        var cards = [CardView]()
        
        var count = 0
        var idx = 0
        
        for idx in 0..<2 {
            cards.append(CardView(car: cars[idx]))
        }

        return cards
        
    }()
    
    @State private var removalTransition = AnyTransition.leadingBottomAtRemoval
    
    @EnvironmentObject var settings: SettingsStorage
       
    @State private var didLoad = false
    @State private var allFavs = false
    
    @State private var menuOpen = false
    
    var body: some View {
        ZStack {
            NavigationView {
                VStack {
                    TopBarView(cars: [Car](), menuOpen: $menuOpen).environmentObject(settings)
                    Spacer()
                    ZStack {
                        Text("Todos los vehículos han sido marcados como favoritos")
                            .font(.system(size: 26, design: .rounded))
                            .fontWeight(.bold)
                            .padding()
                            .multilineTextAlignment(.center)
                            .opacity(allFavs ? 1 : 0)
                        ForEach(deck) { card in
                            card
                                .zIndex(self.isTopCard(card: card) ? 1 : 0)
                                .overlay(
                                    ZStack {
                                        Image(systemName: "xmark")
                                            .font(.system(size: 80))
                                            .foregroundColor(Color(.systemRed))
                                            .opacity(self.dragState.translation.width < -threshold && self.isTopCard(card: card) ? 1 : 0)
                                        
                                        Image(systemName: "heart.fill")
                                            .font(.system(size: 80))
                                            .foregroundColor(Color(.systemRed))
                                            .opacity(self.dragState.translation.width > threshold && self.isTopCard(card: card) ? 1 : 0)
                                    }
                                )
                                .offset(x: self.isTopCard(card: card) ? self.dragState.translation.width : 0,
                                        y: self.isTopCard(card: card) ? self.dragState.translation.height : 0)
                                .scaleEffect(self.isTopCard(card: card) && self.dragState.isDragging ? 0.9 : 1)
                                .rotationEffect(Angle(degrees:
                                                        Double(self.isTopCard(card: card) ? self.dragState.translation.width / 20 : 0)))
                                .animation(.interpolatingSpring(stiffness: 200, damping: 100))
                                .transition(self.removalTransition)
                                .gesture(LongPressGesture(minimumDuration: 0.01)
                                            .sequenced(before: DragGesture())
                                            .updating(self.$dragState, body: { (value, state, transaction) in
                                                switch value {
                                                case .first(true):
                                                    state = .pressing
                                                case .second(true, let drag):
                                                    state = .dragging(translation: drag?.translation ?? .zero)
                                                default:
                                                    break
                                                }
                                            })
                                            .onChanged { (value) in
                                                guard case .second(true, let drag?) = value else { return }
                                                self.removalTransition = drag.translation.width > 0 ? .trailingBottomAtRemoval : .leadingBottomAtRemoval
                                            }
                                            .onEnded { (value) in
                                                guard case .second(true, let drag?) = value else { return }
                                                
                                                //TODO: Marcar el curso como X o como corazón
                                                if drag.translation.width > self.threshold {
                                                    let date = self.getCurrentDate()
                                                    var car = deck[0].car
                                                    car.date = date
                                                    settings.setFavouriteCar(car: car)
                                                    self.updateDeck()
                                                }
                                                
                                                if drag.translation.width < -self.threshold {
                                                    self.updateDeck()
                                                }
                                                
                                            }
                                         
                                )
                            
                        }
                    }
                    
                    .onAppear {
                        if !didLoad {
                            didLoad = true
                            self.prepareInitialDeck()
                        }
                    }

                    Spacer()
                    BottomBarView(car: deck.count > 0 ? deck[0].getCurrentCar() : Car(name: "", image: "", price: 0), buttonTitle: "", allFavs: self.allFavs).environmentObject(settings)
                        .opacity(self.dragState.isDragging ? 0.1 : 1)
                        .animation(.default)
                }
                .navigationBarHidden(true)
            }
            
            ZStack {
                MenuView(width: 280, isOpen: self.menuOpen, menuClose: self.openMenu)
            }
        
        }
        
    }
    
    func openMenu() {
        self.menuOpen.toggle()
    }
    
    private func getCurrentDate() -> String {
        var dateFormatter: DateFormatter {
            let formatter = DateFormatter()
            formatter.dateFormat = "dd-MMM-yyyy - HH:mm"
            return formatter
         }
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        return dateString
    }
    
    private func isTopCard(card: CardView) -> Bool {
        
        guard let idx = deck.firstIndex(where: { $0.id == card.id}) else {
            return false
        }
        //Si llegamos aquí, está en el mazo
        return idx == 0
    }
    
    private func prepareInitialDeck() {
        
        deck.removeAll()
        
        var i = 0
        var idx = 0
        while i < 2 && idx < cars.count {
            let car = cars[idx]
            if !settings.isFavourite(carName: car.name) {
                deck.append(CardView(car: car))
                i += 1
            }
            idx += 1
        }
        self.lastCardIdx = idx - 1

        allFavs = (deck.count == 0) ? true : false
        
    }
    
    private func updateDeck() {
        deck.removeFirst()
        self.lastCardIdx += 1
        
        var newCar = cars[self.lastCardIdx % cars.count]
        var count = 0
        while (settings.isFavourite(carName: newCar.name) && count < cars.count) {
            self.lastCardIdx += 1
            newCar = cars[self.lastCardIdx % cars.count]
            count += 1
        }
        if settings.isFavourite(carName: newCar.name) {
            print("Todos los coches se han marcado como favorito")
            deck.removeAll()
            allFavs = true
        } else {
            let newCardView = CardView(car: newCar)
            deck.append(newCardView)
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            ContentView(currentCar: Car(name: "", image: "", price: 0)).environmentObject(SettingsStorage())
            TopBarView(cars: [Car](), menuOpen:.constant(false)).previewLayout(.sizeThatFits).environmentObject(SettingsStorage())
//            BottomBarView(car: Car(name: "", image: "", price: 0), buttonTitle: "").previewLayout(.sizeThatFits)
        }
    }
}

struct TopBarView: View {
    
    @EnvironmentObject var settings: SettingsStorage
    @State var cars: [Car]
    
    @Binding var menuOpen: Bool
    
    var body: some View {
        
        
            HStack {
                Image(systemName: "line.horizontal.3")
                    .font(.system(size: 25))
                    .foregroundColor(.black)
                    .onTapGesture {
                        menuOpen.toggle()
                    }
                Spacer()
                
                NavigationLink(
                    destination: PurchasedView().environmentObject(settings),
                    label: {
                        Image(systemName: "car.fill")
                            .font(.system(size: 35))
                            .foregroundColor(.black)
                    })
                    
                
                Spacer()
                NavigationLink(
                    destination: FavsView().environmentObject(settings),
                    label: {
                        Image(systemName: "archivebox.fill")
                            .font(.system(size: 25))
                            .foregroundColor(.black)
                    })
                    .onTapGesture {
                        cars = settings.getFavouritesCarsObjects() ?? [Car]()
                    }
            }
            
        
        
        .padding()
    }
    
}

struct BottomBarView: View {

    @State private var showingSheet = false
    var car: Car
    @EnvironmentObject var settings: SettingsStorage
    @State var buttonTitle: String
    @State private var showsAlert = false
    var allFavs: Bool
    
    var body: some View {
        
        HStack {
            
            Image(systemName: "xmark")
                .font(.system(size: 25))
                .foregroundColor(.black)
            
            Spacer()
            
            Button(action: {
                settings.contains(carName: car.name) ? showsAlert.toggle() : showingSheet.toggle()
            }, label: {
                Text("Comprar coche")
                    .bold()
                    .foregroundColor(.white)
                    .padding(20)
                    .background(Color.black)
                    .cornerRadius(12)
            })
            .disabled(allFavs)
            .padding(15)
            .sheet(isPresented: $showingSheet) {
                PurchaseView(car: car).environmentObject(settings)
            }
            .onAppear {
                buttonTitle = settings.contains(carName: car.name) ? "Coche comprado" : "Comprar coche"
            }
            .alert(isPresented: self.$showsAlert) {
                Alert(title: Text("Ya has comprado este coche"))
            }
            
            Spacer()
            
            Image(systemName: "heart")
                .font(.system(size: 25))
                .foregroundColor(.black)
            
        }
        .padding()
        
    }
    
}
