//
//  CardView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 08/05/2021.
//

import SwiftUI

//Implementa el protocolo Identifiable para que pueda recorrerse en un ForEach
struct CardView: View, Identifiable {
    
    let id: UUID = UUID()
    var car: Car
    
    var body: some View {
        
        RoundedRectangle(cornerRadius: 20)
//            .stroke()
            .stroke(lineWidth: 0)
            .background(
                Image(car.image)
                    .resizable()
                    .aspectRatio(contentMode: .fill)
            )
            .cornerRadius(20)
            .padding()
            .frame(height: 350)
            .overlay(
                VStack {
                    Spacer()
                    Text(car.name)
                        .font(.system(.title, design: .rounded))
                        .fontWeight(.bold)
                        .frame(maxWidth: .infinity)
                        .background(Color.black.opacity(0.6))
                        .foregroundColor(.white)
                        .cornerRadius(8)
                        .padding()
                }
                .padding()
        )


    }
    
    func getCurrentCar() -> Car {
        return self.car
    }
}

struct CardView_Previews: PreviewProvider {
    static var previews: some View {
        CardView(car: cars[7])
    }
}

