//
//  P2_TinderApp.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 08/05/2021.
//

import SwiftUI

@main
struct P2_TinderApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(currentCar: Car(name: "", image: "", price: 0)).environmentObject(SettingsStorage())
        }
    }
}
