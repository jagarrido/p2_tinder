//
//  Extensions.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 13/5/21.
//

import SwiftUI

extension AnyTransition {
    
    static var trailingBottomAtRemoval: AnyTransition {
        AnyTransition.asymmetric(insertion: .identity, removal: AnyTransition.move(edge: .trailing).combined(with: .move(edge: .bottom)))
    }
    //identity significa que no hay animación. No hay animación en la inserción, pero al terminar sí hay
    
    static var leadingBottomAtRemoval: AnyTransition {
        AnyTransition.asymmetric(insertion: .identity, removal: AnyTransition.move(edge: .leading).combined(with: .move(edge: .bottom)))
    }
        
}
