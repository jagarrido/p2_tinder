//
//  FavsView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 29/5/21.
//

import SwiftUI

struct FavsView: View {
    
    @EnvironmentObject var settings: SettingsStorage
    
    @State private var cars = [Car]()
    
    private let car = Car(name: "Focus", image: "focus2", price: 20000)
    
    var body: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(self.cars) { car in
                    CarRow(car: car)
                }
            }
        }
        .onAppear {
            cars = settings.getFavouritesCarsObjects() ?? [Car]()
        }
        
        .navigationBarTitle("Coches favoritos")
        
    }
}

struct FavsView_Previews: PreviewProvider {
    static var previews: some View {
        FavsView().environmentObject(SettingsStorage())
    }
}

struct CarRow: View {
    
    var car: Car
    
    var body: some View {
        HStack {
            Image(self.car.image)
                .resizable()
                .aspectRatio(contentMode: .fill)
                .frame(width: 140, height: 100)
                .cornerRadius(20)
                .padding(.leading)
            VStack {
                Text(self.car.name)
                    .font(.system(.title, design: .rounded))
                    .fontWeight(.bold)
                    .frame(maxWidth: .infinity, alignment: .leading)
                    .padding(.trailing, 16)
                HStack {
                    Text("Precio:")
                        .font(.system(.body, design: .rounded))
                        .bold()
                    Text("\(self.car.price) €")
                        .font(.system(.subheadline, design: .rounded))
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
                HStack {
                    Image(systemName: "star.fill")
                        .font(.system(.body, design: .rounded))
                        .foregroundColor(.yellow)
                    Text("\(self.car.date ?? "Fecha")")
                        .font(.system(.subheadline, design: .rounded))
                        .frame(maxWidth: .infinity, alignment: .leading)
                }
            }
        }
    }
}
