//
//  PurchaseView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 15/5/21.
//

import SwiftUI

struct PurchaseView: View {
    
    var car: Car
        
    @EnvironmentObject var settings: SettingsStorage
    
    var body: some View {
        VStack {
            
            Image(car.image)
                .resizable()
                .aspectRatio(contentMode: .fit)
                .cornerRadius(8)
                .padding()
            Text(car.name)
                .font(.system(.title, design: .rounded))
                .bold()
            Divider()
                .padding(.horizontal)
            HStack {
                Spacer()
                Text("Total: \(car.price) €")
                    .padding(.horizontal)
            }
            Button(action: {
                settings.saveCar(carName: car.name)
            }) {
                HStack {
                    Image(systemName: "cart")
                        .font(.title2.weight(.bold))
                    Text("Comprar")
                        .font(.title2)
                        .bold()
                }
                .padding()
                .foregroundColor(.white)
                .background(Color.blue)
                .cornerRadius(8)
            }
            
        }
    }
}

struct PurchaseView_Previews: PreviewProvider {

    static var previews: some View {
        PurchaseView(car: (Car(name: "Focus", image: "focus2", price: 0)))
    }
}
