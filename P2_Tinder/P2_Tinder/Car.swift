//
//  Car.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 08/05/2021.
//

import Foundation

struct Car: Identifiable, Codable {
    var id: UUID = UUID()
    var name: String
    var image: String
    var price: Int
    var purchased: Bool = false
    var date: String?
}

#if DEBUG

var cars: [Car] = [
    Car(name: "Fiesta", image: "fiesta2", price: 10000),
    Car(name: "Focus", image: "focus2", price: 18000),
    Car(name: "Kuga", image: "kuga2", price: 22000),
    Car(name: "Mondeo", image: "mondeo2", price: 26000),
    Car(name: "Mustang Match-E", image: "mustang-match-e2", price: 38000),
    Car(name: "Mustang", image: "mustang2", price: 36000),
    Car(name: "Puma", image: "puma2", price: 22000),
    Car(name: "Ranger", image: "ranger2", price: 45000)
]

#endif

