//
//  PurchasedView.swift
//  P2_Tinder
//
//  Created by Juan Ant. Garrido on 24/5/21.
//

import SwiftUI

struct PurchasedView: View {
    
    @EnvironmentObject var settings: SettingsStorage
    
    var body: some View {
        
        ScrollView {
            LazyVStack {
                ForEach(cars
                            .filter(shouldShowCar)) { car in
                    CardView(car: car)
                }
            }
        }

        .navigationBarTitle("Coches comprados")
        
    }
    
    private func shouldShowCar(car: Car) -> Bool {
        return settings.contains(carName: car.name)
    }

    
}

struct PurchasedView_Previews: PreviewProvider {
    static var previews: some View {
        PurchasedView().environmentObject(SettingsStorage())
    }
}
